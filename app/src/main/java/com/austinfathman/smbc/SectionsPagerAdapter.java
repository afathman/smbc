package com.austinfathman.smbc;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

/**
 * Created by afathman on 11/14/15.
 */
public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    ComicView mActivity;
    String TAG = "SectionsPagerAdapter";

    public SectionsPagerAdapter(FragmentManager fm, ComicView activity) {
        super(fm);
        mActivity = activity;

    }


    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a ComicFragment
        ComicFragment myFragment = ComicFragment.newInstance(position + 1);
        return myFragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    @Override
    public int getCount() {
        Log.v(TAG, "Last comic: " + mActivity.last_comic);
        return mActivity.last_comic;
    }

    public void setLastComic(Integer lastComicID) {
        mActivity.last_comic = lastComicID;
        SharedPreferences sharedPref = mActivity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(mActivity.getString(R.string.last_comic_preference), lastComicID);
        editor.commit();
        notifyDataSetChanged();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mActivity.getString(R.string.title);
    }

}

