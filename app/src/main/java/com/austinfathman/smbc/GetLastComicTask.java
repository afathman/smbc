package com.austinfathman.smbc;

import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by afathman on 11/14/15.
 */
class GetLastComicTask extends AsyncTask<String, Void, Document> {

    ViewPager mViewPager;
    ComicView mContext;

    GetLastComicTask(ViewPager viewPager, ComicView context) {
        mViewPager = viewPager;
        mContext = context;
    }

    protected Document doInBackground(String... urls) {
        Document doc = null;
        try {
            doc = Jsoup.connect("http://smbc-comics.com/index.php").get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }

    protected void onPostExecute(Document html) {
        if (html != null) {
            String previousLink = html.select("#comicleft").select("a").get(1).attr("href");
            if (previousLink != "") {
                ((SectionsPagerAdapter) mViewPager.getAdapter()).setLastComic(Integer.parseInt(previousLink.substring(previousLink.indexOf("=") + 1)) + 1);
                if (mViewPager.getCurrentItem() != mContext.last_comic){
                    mViewPager.setCurrentItem(mContext.last_comic, true);
                }
            }

        } else {
            Toast.makeText(mContext, "Could not load latest comic.", Toast.LENGTH_LONG).show();
        }


    }

}