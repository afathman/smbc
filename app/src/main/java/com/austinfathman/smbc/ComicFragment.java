package com.austinfathman.smbc;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by afathman on 11/14/15.
 */
public class ComicFragment extends Fragment {
    String TAG = "ComicFragment";
    private String smbc_url = "http://smbc-comics.com/";

    private RetrieveHTMLTask mTask;
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

        } else {
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTask != null){
            mTask.cancel(true);
        }
        if (comic != null) {
            Drawable d = comic.getDrawable();
            if (d instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) d;
                Bitmap bitmap = bitmapDrawable.getBitmap();
                bitmap.recycle();
            }
        }
    }


    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_URL = "section_url";
    private static final String SMBC_INDEX_URL = "http://smbc-comics.com/index.php?id=";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ComicFragment newInstance(int sectionNumber) {
        String url = SMBC_INDEX_URL + sectionNumber;
        ComicFragment fragment = new ComicFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    public ComicFragment() {
    }

    private ImageView comic;
    private ImageView extraComic;
    boolean extraComicVisible;

    public void toggleExtraComic() {
        if (extraComic != null) {
            if (extraComicVisible) {
                extraComic.setVisibility(View.GONE);

            } else {
                extraComic.setVisibility(View.VISIBLE);

            }
            extraComicVisible = !extraComicVisible;
        } else {
            Log.e(TAG, "Extra comic failed to load.");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Parse html to get image dimensions, images, and alt text.
        final String url = getArguments().getString(ARG_URL);

        View rootView = inflater.inflate(R.layout.fragment_comic_view, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.date);
        final ProgressBar bar = (ProgressBar) rootView.findViewById(R.id.progress);
        final TextView errorText = (TextView) rootView.findViewById(R.id.errorText);
        textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

        comic = (ImageView) rootView.findViewById(R.id.comic);

        extraComic = (ImageView) rootView.findViewById(R.id.extracomic);
        extraComic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleExtraComic();
            }
        });
        errorText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
                mTask = new RetrieveHTMLTask(comic, extraComic, bar, errorText);
                mTask.execute(url);
            }
        });

        mTask = new RetrieveHTMLTask(comic, extraComic, bar, errorText);
        mTask.execute(url);

        return rootView;
    }


    class RetrieveHTMLTask extends AsyncTask<String, Integer, Document> {

        private ImageView comic;
        private ImageView extracomic;
        private ProgressBar mBar;
        private TextView mErrorText;

        RetrieveHTMLTask(ImageView c, ImageView ec, ProgressBar bar, TextView errorText) {
            comic = c;
            extracomic = ec;
            extraComic.bringToFront();
            mBar = bar;
            mErrorText = errorText;
        }

        protected Document doInBackground(String... urls) {
            Document doc = null;
            try {
                doc = Jsoup.connect(urls[0]).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return doc;
        }

        protected void onPostExecute(Document html) {
            if (html != null) {
                String mainComicURL = html.select("#comic").attr("src");
                String extraComicURL = html.select("#aftercomic").select("img").attr("src");
                final String comicExtraText = html.select("#comic").attr("title");
                mainComicURL = mainComicURL.replace(" ", "%20");
                extraComicURL = extraComicURL.replace(" ", "%20");

                if (mainComicURL != null && !mainComicURL.equals("")) {

                    mBar.setVisibility(View.VISIBLE);


                    DisplayImageOptions mainOptions = new DisplayImageOptions.Builder()
                            .cacheOnDisk(true)
                            .bitmapConfig(Bitmap.Config.RGB_565)
                            .imageScaleType(ImageScaleType.NONE)
                            .build();

                    ImageLoader.getInstance().displayImage(smbc_url + mainComicURL, comic, mainOptions, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            mBar.setVisibility(View.VISIBLE);
                        }


                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            String message = null;
                            switch (failReason.getType()) {
                                case IO_ERROR:
                                    message = "Input/Output error";
                                    break;
                                case DECODING_ERROR:
                                    message = "Image can't be decoded";
                                    break;
                                case NETWORK_DENIED:
                                    message = "Downloads are denied";
                                    break;
                                case OUT_OF_MEMORY:
                                    message = "Out Of Memory error";
                                    break;
                                case UNKNOWN:
                                    message = "Unknown error";
                                    break;
                            }
                            Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();


                            mBar.setVisibility(View.GONE);
                        }


                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            mBar.setVisibility(View.GONE);
                        }
                    });

                    DisplayImageOptions extraOptions = new DisplayImageOptions.Builder()
                            .cacheOnDisk(true)
                            .imageScaleType(ImageScaleType.EXACTLY)
                            .build();
                    ImageLoader.getInstance().displayImage(extraComicURL, extracomic, extraOptions);

                    comic.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {

                            Toast.makeText(getContext(), comicExtraText, Toast.LENGTH_LONG).show();
                            return true;
                        }
                    });
                } else {
                    Toast.makeText(getContext(), getString(R.string.network_or_parse_error), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.network_or_parse_error), Toast.LENGTH_LONG).show();
            }


        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

    }


}
