package com.austinfathman.smbc;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class ComicView extends AppCompatActivity {

    private Tracker mTracker;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    ImageView button;
    ImageView first;
    ImageView prev;
    ImageView next;
    ImageView last;
    ComicView mActivity;
    public Integer last_comic = 3933;
    String TAG = "ComicView";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic_view);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), this);
        mActivity = this;

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        last_comic = sharedPref.getInt(getString(R.string.last_comic_preference), last_comic);


        mTracker = getDefaultTracker();
        mTracker.setScreenName(TAG);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_comic_view);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.action_about) {
                    buttonClickAnalytics(getString(R.string.analytics_about));
                    AlertDialog alertDialog = new AlertDialog.Builder(mActivity).setView(R.layout.dialog_about).create();
                    alertDialog.setTitle(getString(R.string.title));
                    alertDialog.show();
                    alertDialog.findViewById(R.id.about_view_link).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                           openInBrowser("");
                        }
                    });
                    return true;
                } else if (id == R.id.action_goto) {
                    buttonClickAnalytics(getString(R.string.analytics_goto));
                    AlertDialog alertDialog = new AlertDialog.Builder(mActivity).setView(R.layout.dialog_goto_comic).create();
                    alertDialog.setTitle(getString(R.string.action_goto));
                    alertDialog.setMessage(getString(R.string.enter_comic_number));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.go),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    EditText gotoComic = (EditText) ((Dialog) dialog).findViewById(R.id.goto_comic_editText);
                                    if (gotoComic.getText().toString().length() > 0) {
                                        int comicNumber = Math.abs(Integer.parseInt(gotoComic.getText().toString()));
                                        if (comicNumber <= last_comic && comicNumber != 0) {
                                            mViewPager.setCurrentItem(comicNumber - 1);
                                            dialog.dismiss();
                                        } else {
                                            Toast.makeText(mActivity, getString(R.string.comic_out_of_bounds), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                    return true;
                } else if (id == R.id.action_open_in_browser){
                    buttonClickAnalytics(getString(R.string.analytics_open_in_browser));
                    openInBrowser(String.valueOf(mViewPager.getCurrentItem()+1));
                }else if(id == R.id.action_share){
                    buttonClickAnalytics(getString(R.string.analytics_share));
                    try
                    { Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        //i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                        String link = getString(R.string.about_section_link) + getString(R.string.comic_id_prefix) + String.valueOf(mViewPager.getCurrentItem()+1);
                        i.putExtra(Intent.EXTRA_TEXT, link);
                        startActivity(Intent.createChooser(i, getString(R.string.share_with)));
                    }
                    catch(Exception e)
                    {}
                }
                return false;
            }

        });

        ImageView patreon = (ImageView) findViewById(R.id.patreon);
        patreon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClickAnalytics(getString(R.string.analytics_patreon));
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.patreon_url)));
                startActivity(browserIntent);
            }
        });

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();

        ImageLoader.getInstance().init(config);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(1);

        Uri data = getIntent().getData();
        if (data!=null){
            String url = data.toString();
            Log.d(TAG, url);
            if (url.contains(getString(R.string.comic_id_prefix))){
                String dirty_id = url.split("=")[1];

                if (!dirty_id.matches("[0-9]+")) {
                    for (int i = 0; i < dirty_id.length(); i++) {
                        if (!dirty_id.substring(i,i+1).matches("[0-9]")) {
                            dirty_id = dirty_id.substring(0,i);
                            break;
                        }
                    }
                }
                Integer id = Integer.parseInt(dirty_id)-1;
                id = Math.max(0,id);
                Log.d(TAG,id.toString());
                mViewPager.setCurrentItem(id, true);

            }else{
                new GetLastComicTask(mViewPager, mActivity).execute();
                Log.d(TAG,"Could not find: " + getString(R.string.comic_id_prefix));
            }
        }else{
            new GetLastComicTask(mViewPager, mActivity).execute();
        }




        button = (ImageView) findViewById(R.id.button);
        first = (ImageView) findViewById(R.id.first);
        prev = (ImageView) findViewById(R.id.prev);
        next = (ImageView) findViewById(R.id.next);
        last = (ImageView) findViewById(R.id.last);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClickAnalytics(getString(R.string.analytics_red_button));
                ComicFragment currentFrag = getCurrentFragment();
                if (currentFrag != null) {
                    currentFrag.toggleExtraComic();
                }
            }
        });
        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClickAnalytics(getString(R.string.analytics_first));
                mViewPager.setCurrentItem(0, true);
            }
        });
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClickAnalytics(getString(R.string.analytics_prev));
                mViewPager.setCurrentItem(Math.max(mViewPager.getCurrentItem() - 1, 0), true);
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClickAnalytics(getString(R.string.analytics_next));
                mViewPager.setCurrentItem(Math.min(mViewPager.getCurrentItem() + 1, last_comic), true);
            }
        });
        last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClickAnalytics(getString(R.string.analytics_last));
                new GetLastComicTask(mViewPager, mActivity).execute();
                mViewPager.setCurrentItem(last_comic, true);
            }
        });


    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.enableAutoActivityReports(getApplication());
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }


    public ComicFragment getCurrentFragment() {
        return (ComicFragment) mSectionsPagerAdapter.instantiateItem(mViewPager, mViewPager.getCurrentItem());
    }

    private void buttonClickAnalytics(String buttonName){
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Click")
                .setAction(buttonName)
                .build());
    }

    private void openInBrowser(String comicNumber){
        Intent i = new Intent(Intent.ACTION_VIEW,
                Uri.parse(comicNumber.length()>0?getString(R.string.about_section_link)+getString(R.string.comic_id_prefix)+comicNumber:
                        getString(R.string.about_section_link)));

        // Check what is the default app to handle these entry URLs
        ResolveInfo defaultResolution = getPackageManager().resolveActivity(i,
                PackageManager.MATCH_DEFAULT_ONLY);
        // If this app is the default app for entry URLs, use the browser instead
        if (defaultResolution.activityInfo.packageName.equals(getPackageName())) {
            Intent browseIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"));
            ResolveInfo browseResolution = getPackageManager().resolveActivity(browseIntent,
                    PackageManager.MATCH_DEFAULT_ONLY);
            i.setComponent(new ComponentName(
                    browseResolution.activityInfo.applicationInfo.packageName,
                    browseResolution.activityInfo.name));

        }

        startActivity(i);
    }

}
