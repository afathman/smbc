**Known Issues:**

No handling of missing comics (eg. comic id #4).

I hate RSS feeds, so all of this works by parsing html files.

Need better comic handling when user is offline.


**Possible future improvements**

Thumbnail browsing

Finding where Zach keeps the comic titles other than the RSS feed.

Random comic

Notification when a new comic is posted

Handle links in comics (fundraiser, Christmas card, etc.)

-----------------------------------------------

Feel free to make any pull requests. I'll periodically make some improvements when a bug inconveniences me.